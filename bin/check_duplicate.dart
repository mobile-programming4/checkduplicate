import 'dart:io';

void main() {
  int count = 1;
  String? message =
      "Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences, a paragraph is half a page long, etc. In reality, though, the unity and coherence of ideas among sentences is what constitutes a paragraph. A paragraph is defined as “a group of sentences or a single sentence that forms a unit” (Lunsford and Connors 116). Length and appearance do not determine whether a section in a paper is a paragraph. For instance, in some styles of writing, particularly journalistic styles, a paragraph can be just one sentence long. Ultimately, a paragraph is a sentence or group of sentences that support one main idea. In this handout, we will refer to this as the “controlling idea,” because it controls what happens in the rest of the paragraph.";
  var chagetoLow = message.toLowerCase();
  var words = chagetoLow.split(" ");
  List<String> wordsList = [];
  List<int> count_word = [];
  for (int i = 0; i < words.length; i++) {
    var duplicate = false;
    for (int j = 0; j < wordsList.length; j++) {
      if (words[i] == wordsList[j]) {
        duplicate = true;
        count_word[j] = count_word[j] + 1;
        break;
      }
    }
    if (duplicate == false) {
      wordsList.add(words[i]);
      count_word.add(1);
    }
  }
  for (var i = 0; i < wordsList.length; i++) {
    print("${wordsList[i]} ${count_word[i]}");
  }
}
